package edu.buffalo.cse116;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class WeaponGuessEventHandler implements ActionListener {
	private Player _p;
	private Card _c;
	public WeaponGuessEventHandler(Card c, Player p){
		_c = c;
		_p = p;
	}
	@Override
	public void actionPerformed(ActionEvent arg0) {
		if (_p.getPlayerGuessing()){
			_p.WeaponGuessing(_c);
		}
	}

}
