package edu.buffalo.cse116;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class SuspectFinalEventHandler implements ActionListener {
	private Player _p;
	private Card _c;
	public SuspectFinalEventHandler(Card c, Player p){
		_p = p;
		_c = c;
	}
	@Override
	public void actionPerformed(ActionEvent arg0) {
		_p.setFinalSuspect(_c);
	}

}
