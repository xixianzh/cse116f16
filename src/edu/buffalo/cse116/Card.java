package edu.buffalo.cse116;

public class Card {
	
	private String _name;
	private String _type;
	
	public Card(String type,String name){
		_type = type;
		_name = name;}
		
	
	public String getType(){return _type;}
	
	
	public String getName(){return _name;}
}
