package edu.buffalo.cse116;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class LeftButtonEventHandler implements ActionListener {
	private Player _p;
	public LeftButtonEventHandler(Player p){
		_p = p;
	}
	public void actionPerformed(ActionEvent e) {
		if (_p.getMovesLeft() != 0){
			if (_p.getr1() == 0 && _p.getc1() == 0){
				_p.setJWalk(false);
				_p.setMovesLeft(_p.getMovesLeft() - 1);
				_p.playerMove(7, 7);
			}
			else if (_p.getr1() == 7 && _p.getc1() == 0){
				_p.setJWalk(false);
				_p.setMovesLeft(_p.getMovesLeft() - 1);
				_p.playerMove(0, 7);
			}
			else if (_p.getc1() == 0){
				_p.setJWalk(true);
			}
			else if (!_p.playerMove(_p.getr1(), _p.getc1() - 1)){
				_p.setJWalk(true);
			}
			else {
				_p.setJWalk(false);
				_p.setMovesLeft(_p.getMovesLeft() - 1);
			}
			_p.setFrozen(false);
			_p.setUnfinishedBusiness(false);
			_p.setJumpingTheGun(false);
		}
		else {
			_p.setFrozen(true);
		}
	}

}
