package edu.buffalo.cse116;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class GuessEventHandler implements ActionListener{
	private Player _p;
	public GuessEventHandler(Player p){
		_p = p;
	}	
	public void actionPerformed(ActionEvent e){
		if (_p.getMovesLeft() != 0){
			_p.setUnfinishedBusiness(true);
		}
		else{
			_p.setUnfinishedBusiness(false);
			if (_p.getOneGuessLeft()){
				_p.setPlayerGuessing(true);
				_p.setOneGuessLeft(false);
				}
			_p.setJWalk(false);
		}
		_p.setJumpingTheGun(false);
	}
}
