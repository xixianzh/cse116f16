package edu.buffalo.cse116;

import java.util.ArrayList;

public class Player {

	private int _r1;
	private int _c1;
	private double[][] _board;
	private ArrayList<Card> _pCards;
	int _index;
	int _currentRoll;
	private ArrayList<Observer> _observers;
	private int _movesLeft;	
	private Boolean _displayCards;
	private Boolean _playerGuessing;
	private Card _weaponGuess;
	private Card _suspectGuess;
	private boolean _jWalk;
	private boolean _frozen;
	private boolean _hallwayGuessin;
	private boolean _unfinishedBusiness;
	private boolean _oneGuessLeft;
	private boolean _finallyGuessing;
	private boolean _updateBizarro;
	private boolean _tornACL;
	private boolean _jumpingTheGun;
	private boolean _heGone;
	private Card _finalWeapon;
	private Card _finalSuspect;
	private Card _finalRoom;
	
	public Player(double[][] board, int r1, int c1, ArrayList<Card> pc){
		_board = board;
		_r1 = r1;
		_c1 = c1;
		_pCards = pc;
		_index = 0;
		_observers = new ArrayList<Observer>();
		_playerGuessing = false;
		_suspectGuess = null;
		_weaponGuess = null;
		_jWalk = false;
		_frozen = false;
		_hallwayGuessin = false;
		_unfinishedBusiness = false;
		_oneGuessLeft = true;
		_finallyGuessing = false;
		_updateBizarro = false;
		_tornACL = false;
		_jumpingTheGun = false;
		_heGone = false;
		
		_finalWeapon = null;
		_finalSuspect = null;
		_finalRoom = null;
	}
	
	public void setRoll(int roll){
		_currentRoll = roll;
	}
	
	public int getRoll(){
		return _currentRoll;
	}
	
	public void setIndex(int i){
		_index = i;
	}
	
	public Card cardShow(Card r_card, Card w_card, Card s_card){
		for (Card c: _pCards){
			if (c.getName().equals(r_card.getName())){return r_card;}
			else if (c.getName().equals(w_card.getName())){return w_card;}
			else if (c.getName().equals(s_card.getName())){return s_card;}
		}
		return null;
	}
	
    public void gridInitialization(){
	   _board = new double[8][8];
	   for(int i = 0; i < 8; i++){
		   _board[i][2] = 0;
		   _board[i][5] = 0;
		   _board[2][i] = 0;
		   _board[5][i] = 0;
	   }
	   _board[0][0] = 1.1; 
	   _board[0][1] = 1;
	   _board[1][0] = 1;
	   _board[1][1] = -1;
	   _board[0][3] = 2;
	   _board[0][4] = 2;
	   _board[1][4] = 2;
	   _board[1][3] = -2;
	   _board[0][6] = 3;
	   _board[0][7] = 3.1;
	   _board[1][7] = 3;
	   _board[1][6] = -3;
	   _board[3][0] = 4;
	   _board[3][1] = 4;
	   _board[4][0] = 4;
	   _board[4][1] = -4;
	   _board[3][3] = -51;
	   _board[3][4] = -52;
	   _board[4][3] = -53;
	   _board[4][4] = -54;
	   _board[3][7] = 6;
	   _board[4][7] = 6;
	   _board[4][6] = 6;
	   _board[3][6] = -6;
	   _board[6][0] = 7;
	   _board[7][0] = 7.1;
	   _board[7][1] = 7;
	   _board[6][1] = -7;
	   _board[6][3] = 8;
	   _board[7][3] = 8;
	   _board[7][4] = 8;
	   _board[6][4] = -8;
	   _board[6][7] = 9;
	   _board[7][6] = 9;
	   _board[7][7] = 9.1;
	   _board[6][6] = -9;
	   _board[2][1] = 10;
	   _board[1][2] = 20; 
	   _board[2][3] = 30;
	   _board[3][2] = 40;
	   _board[2][4] = 50; 
	   _board[1][5] = 60;
	   _board[2][6] = 70;
	   _board[3][5] = 80;
	   _board[5][1] = 90;
	   _board[4][2] = 100;
	   _board[5][3] = 110;
	   _board[6][2] = 120;
	   _board[5][4] = 130;
	   _board[4][5] = 140;
	   _board[5][6] = 150;
	   _board[6][5] = 160;
   }
    
	public boolean playerMove(int r2, int c2){
		gridInitialization();
		double start = _board[_r1][_c1];
		if (start == 0){
			if (_board[r2][c2] >= 10){
				_r1 = r2;
				_c1 = c2;
				notifyObservers();
				return true;
			}
			else{
				return false;
			}
		}
		//below: move from door way into room or into hallway
		if (start < 0){
			if ((_board[r2][c2] == -1*start)||(_board[r2][c2] >= 10)){
				_r1 = r2;
				_c1 = c2;
				notifyObservers();
				return true;
			}
			else{
				return false;
			}
		}
		//below: moving between secret passageways 1 and 2
		if (start == 1.1){
			if (_board[r2][c2] == 9.1){
				_r1 = r2;
				_c1 = c2;
				notifyObservers();
				return true;
			}
		}
		if (start == 9.1){
			if (_board[r2][c2] == 1.1){
				_r1 = r2;
				_c1 = c2;
				notifyObservers();
				return true;
			}
		}
		if (start == 3.1){
			if (_board[r2][c2] == 7.1){
				_r1 = r2;
				_c1 = c2;
				notifyObservers();
				return true;
			}
		}
		if (start == 7.1){
			if (_board[r2][c2] == 3.1){
				_r1 = r2;
				_c1 = c2;
				notifyObservers();
				return true;
			}
		}
		//below: moving within room to another spot in room (i.e. secret passageway, into doorway)
		if ((start > 0)&&(start < 10)){
			if (_board[r2][c2] == -1*start || _board[r2][c2] == start + 0.1 || _board[r2][c2] == start - 0.1 || _board[r2][c2] == start){
				_r1 = r2;
				_c1 = c2;
				notifyObservers();
				return true;
			}
			else if (_board[r2][c2] == start + 4 || _board[r2][c2] == start - 4 || _board[r2][c2] == start + 4 || _board[r2][c2] == start - 4){
					_r1 = r2;
					_c1 = c2;
					notifyObservers();
					return true;
			}
			else{
				return false;
			}
		}
		// below: can't move diagonally, specifically in the hallways
		if (start == 10){
			if ((_board[r2][c2] == 0)||(_board[r2][c2] == -1)){
				_r1 = r2;
				_c1 = c2;
				notifyObservers();
				return true;
			}
			else{
				return false;
			}
		}
		if (start == 20){
			if ((_board[r2][c2] == 0)||(_board[r2][c2] == -1)||(_board[r2][c2] == -2)){
				_r1 = r2;
				_c1 = c2;
				notifyObservers();
				return true;
			}
			else{
				return false;
			}
		}
		if (start == 30){
			if ((_board[r2][c2] == 0)||(_board[r2][c2] == -2)||(_board[r2][c2] == -51)||(_board[r2][c2] == 50)){
				_r1 = r2;
				_c1 = c2;
				notifyObservers();
				return true;
			}
			else{
				return false;
			}
		}
		if (start == 40){
			if ((_board[r2][c2] == 0)||(_board[r2][c2] == 100)||(_board[r2][c2] == -51)){
				_r1 = r2;
				_c1 = c2;
				notifyObservers();
				return true;
			}
			else{
				return false;
			}
		}
		if (start == 50){
			if ((_board[r2][c2] == 0)||(_board[r2][c2] == -52)||(_board[r2][c2] == 30)){
				_r1 = r2;
				_c1 = c2;
				notifyObservers();
				return true;
			}
			else{
				return false;
			}
		}
		if (start == 60){
			if ((_board[r2][c2] == 0)||(_board[r2][c2] == -3)){
				_r1 = r2;
				_c1 = c2;
				notifyObservers();
				return true;
			}
			else{
				return false;
			}
		}
		if (start == 70){
			if ((_board[r2][c2] == 0)||(_board[r2][c2] == -3)||(_board[r2][c2] == -6)){
				_r1 = r2;
				_c1 = c2;
				notifyObservers();
				return true;
			}
			else{
				return false;
			}
		}
		if (start == 80){
			if ((_board[r2][c2] == 0)||(_board[r2][c2] == -52)||(_board[r2][c2] == -6)||(_board[r2][c2] == 140)){
				_r1 = r2;
				_c1 = c2;
				notifyObservers();
				return true;
			}
			else{
				return false;
			}
		}
		if (start == 90){
			if ((_board[r2][c2] == 0)||(_board[r2][c2] == -4)||(_board[r2][c2] == -7)){
				_r1 = r2;
				_c1 = c2;
				notifyObservers();
				return true;
			}
			else{
				return false;
			}
		}
		if (start == 100){
			if ((_board[r2][c2] == 0)||(_board[r2][c2] == -4)||(_board[r2][c2] == -53)||(_board[r2][c2] == 40)){
				_r1 = r2;
				_c1 = c2;
				notifyObservers();
				return true;
			}
			else{
				return false;
			}
		}
		if (start == 110){
			if ((_board[r2][c2] == 0)||(_board[r2][c2] == -53)||(_board[r2][c2] == 130)){
				_r1 = r2;
				_c1 = c2;
				notifyObservers();
				return true;
			}
			else{
				return false;
			}
		}
		if (start == 120){
			if ((_board[r2][c2] == 0)||(_board[r2][c2] == -7)){
				_r1 = r2;
				_c1 = c2;
				notifyObservers();
				return true;
			}
			else{
				return false;
			}
		}
		if (start == 130){
			if ((_board[r2][c2] == 0)||(_board[r2][c2] == -8)||(_board[r2][c2] == -54)||(_board[r2][c2] == 110)){
				_r1 = r2;
				_c1 = c2;
				notifyObservers();
				return true;
			}
			else{
				return false;
			}
		}
		if (start == 140){
			if ((_board[r2][c2] == 0)||(_board[r2][c2] == -54)||(_board[r2][c2] == 80)){
				_r1 = r2;
				_c1 = c2;
				notifyObservers();
				return true;
			}
			else{
				return false;
			}
		}
		if (start == 150){
			if (_board[r2][c2] == 6 || _board[r2][c2] == 9){
				return false;
			}
		}
		if (start == 160){
			if ((_board[r2][c2] == 0)||(_board[r2][c2] == -8)||(_board[r2][c2] == -9)){
				_r1 = r2;
				_c1 = c2;
				notifyObservers();
				return true;
			}
			else{
				return false;
			}
		}

		_r1 = r2;
		_c1 = c2;
		notifyObservers();
		return true;
	}
	
	public ArrayList<Card> getCards(){
		return _pCards;
	}
	
	public int getr1(){
		return _r1;
	}
	
	public int getc1(){
		return _c1;
	}
	
	public int getIndex(){
		return _index;
	}
	public void playerMoved(){
		notifyObservers();
	}
	public void notifyObservers() {
		for (Observer obs : _observers) {
			obs.update();
		}
	}
	public void addObserver(Observer obs) {
		_observers.add(obs);
	}
	public void setMovesLeft(int movesLeft){
		_movesLeft = movesLeft;
		notifyObservers();
	}
	public int getMovesLeft(){
		return _movesLeft;
	}
	public boolean getDisplayCards(){
		return _displayCards;
	}
	public void setDisplayCards(boolean displayCards){
		_displayCards = displayCards;
	}
	public void WeaponGuessing(Card c){
		_weaponGuess = c;
		notifyObservers();
	}
	public void suspectGuessing(Card c){
		_suspectGuess = c;
		notifyObservers();
	}
	public void setPlayerGuessing(Boolean b){
		if (b == false){_playerGuessing = b;}
		
		else if (!(_board[_r1][_c1] > 0 && _board[_r1][_c1] < 10)){
			_hallwayGuessin = true;
		}
		else{
			_hallwayGuessin = false;
			_playerGuessing = b;
			notifyObservers();
		}
	}
	public Card getWeaponGuess(){
		return _weaponGuess;
	}
	public Card getSuspectGuess(){
		return _suspectGuess;
	}
	public Boolean getPlayerGuessing(){
		return _playerGuessing;
	}
	public Card roomAt(){
		double location = _board[_r1][_c1];
		if (location == 1.0 || location == 1.1){return new Card("room", "conservatory");}
		else if (location == 2){return new Card("room", "billiard room");}
		else if (location == 3 || location == 3.1){return new Card("room", "kitchen");}
		else if (location == 4){return new Card("room", "library");}
		else if (location == 6){return new Card("room", "dining room");}
		else if (location == 7 || location == 7.1){return new Card("room", "lounge");}
		else if (location == 8){return new Card("room", "ballroom");}
		else if (location == 9 || location == 9.1){return new Card("room", "study");}
		else{return null;}
	}
	public void setPCards(ArrayList<Card> pc){
		_pCards = pc;
	}
	public void setJWalk(boolean b){
		_jWalk = b;
		notifyObservers();
	}
	public boolean getJWalk(){
		return _jWalk;
	}
	public void setFrozen(boolean b){
		_frozen = b;
		_hallwayGuessin = false;
		notifyObservers();
	}
	public boolean getFrozen(){
		return _frozen;
	}
	public void setHallwayGuessin(boolean b){
		_hallwayGuessin = b;
		notifyObservers();
	}
	public boolean getHallwayGuessin(){
		return _hallwayGuessin;
	}
	public void setUnfinishedBusiness(boolean b){
		_unfinishedBusiness = b;
		notifyObservers();
	}
	public boolean getUnfinishedBusiness(){
		return _unfinishedBusiness;
	}
	public void setOneGuessLeft(boolean b){
		_oneGuessLeft = b;
	}
	public boolean getOneGuessLeft(){
		return _oneGuessLeft;
	}
	public void setFinallyGuessing(boolean b){
		_finallyGuessing = b;
		notifyObservers();
	}
	public boolean getFinallyGuessing(){
		return _finallyGuessing;
	}
	public void setFinalWeapon(Card c){
		_updateBizarro = true;
		_finalWeapon = c;
		notifyObservers();
	}
	public Card getFinalWeapon(){
		return _finalWeapon;
	}
	public void setFinalRoom(Card c){
		_updateBizarro = true;
		_finalRoom = c;
		notifyObservers();
	}
	public Card getFinalRoom(){
		return _finalRoom;
	}
	public void setFinalSuspect(Card c){
		_updateBizarro = true;
		_finalSuspect = c;
		notifyObservers();
	}
	public Card getFinalSuspect(){
		return _finalSuspect;
	}
	public void setUpdateBizarro(boolean b){
		_updateBizarro = b;
	}
	public boolean getUpdateBizarro(){
		return _updateBizarro;
	}	
	public void baseballBatToTheKnees(Boolean hitOrMiss){
		_tornACL = hitOrMiss;
	}
	public void baseballBatToTheKnees(boolean hitOrMiss){
		_tornACL = hitOrMiss;
	}
	public boolean kneeCheckup(){
		return _tornACL;
	}
	public void setJumpingTheGun(boolean b){
		_jumpingTheGun = b;
		notifyObservers();
	}
	public boolean getJumpingTheGun(){
		return _jumpingTheGun; 
	}
	public void setHeGone(boolean b){
		_heGone = b;
	}
	public boolean getHeGone(){
		return _heGone;
	}
}
