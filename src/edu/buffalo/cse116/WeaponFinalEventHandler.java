package edu.buffalo.cse116;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class WeaponFinalEventHandler implements ActionListener {
	private Player _p;
	private Card _c;
	public WeaponFinalEventHandler(Card c, Player p){
		_p = p;
		_c = c;
	}
	@Override
	public void actionPerformed(ActionEvent arg0) {
			_p.setFinalWeapon(_c);
	}

}
