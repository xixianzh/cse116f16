package edu.buffalo.cse116;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class SuspectGuessEventHandler implements ActionListener {
	private Card _c;
	private Player _p;
	public SuspectGuessEventHandler(Card c, Player p){
		_p = p;
		_c = c;
	}
	@Override
	public void actionPerformed(ActionEvent arg0) {
		if (_p.getPlayerGuessing()){
			_p.suspectGuessing(_c);
		}
	}

}
