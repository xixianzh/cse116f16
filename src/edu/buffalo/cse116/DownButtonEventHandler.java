package edu.buffalo.cse116;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;

public class DownButtonEventHandler implements ActionListener {
	private Player _p;
	public DownButtonEventHandler(Player p){
		_p = p;
	}
	public void actionPerformed(ActionEvent e) {
		if (_p.getMovesLeft() != 0){
			if (_p.getr1() == 7){
				_p.setJWalk(true);
			}
			else if (!_p.playerMove(_p.getr1() + 1, _p.getc1())){
				_p.setJWalk(true);
			}
			else {
				_p.setJWalk(false);
				_p.setMovesLeft(_p.getMovesLeft() - 1);
			}
			_p.setFrozen(false);
			_p.setUnfinishedBusiness(false);
			_p.setJumpingTheGun(false);
		}
		else {
			_p.setFrozen(true);
		}
	}

}
