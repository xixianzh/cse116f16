
package edu.buffalo.cse116;
import java.awt.BorderLayout;
import java.util.Random;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import edu.buffalo.cse116.Observer;

import javax.swing.*;

public class GameBoardGUI implements Runnable, Observer{
	
	private JPanel _boardPanel;
	private JPanel _menuPanel;
	private JPanel _textPanel;
	private JPanel _weaponsPanel;
	private JPanel _suspectsPanel;
	private JFrame _window;
	private JPanel _floor;
	private double[][] _board;
	private JLabel _p1Label;
	private JLabel _p2Label;
	private JLabel _p3Label;
	private JLabel _p4Label;
	private JLabel _p5Label;
	private JLabel _p6Label;
	private JButton _die;
	private JButton _displayCards;
	private JButton Guess;
	private int _playerTurn;
	private JButton _up;
	private JButton _down;
	private JButton _left;
	private JButton _right;
	private JButton _finalAcc;
	private JPanel _direction;
	private UpButtonEventHandler _ueh;
	private DownButtonEventHandler _deh;
	private LeftButtonEventHandler _leh;
	private RightButtonEventHandler _reh;
	private GuessEventHandler _guessHandler;
	private int _run;
	private int _alRun;
	private JPanel _movesLeftPanel;
	private JFrame _displayCardsWindow;
	private JLabel _roomLabel;
	private JFrame jennifer;
	
	private Card w_solution;
	private Card r_solution;
	private Card s_solution;
	
	private Card _revealingCard;
	
	private Card w_candleStick;
	private Card w_rope;
	private Card w_wrench;
	private Card w_leadPipe;
	private Card w_revolver;
	private Card w_knife;
	private Card w_poisonDartFrog;
	private Card r_billiardRoom;
	private Card r_kitchen;
	private Card r_conservatory;
	private Card r_diningRoom;
	private Card r_lounge;
	private Card r_ballroom;
	private Card r_study;
	private Card r_library;
	private Card s_missScarlet;
	private Card s_colonelMustard;
	private Card s_mrGreen;
	private Card s_mrsWhite;
	private Card s_mrsPeacock;
	private Card s_professorPlum;
		
	private JButton wb;
	private JButton wb1;
	private JButton wb2;
	private JButton wb3;
	private JButton wb4;
	private JButton wb5;
	private JButton wb6;
	
	private Player _player1;
	private Player _player2;
	private Player _player3;
	private Player _player4;
	private Player _player5;
	private Player _player6;
	
	private ArrayList<Card> _cards;
	private ArrayList<Card> _p1cards;
	private ArrayList<Card> _p2cards;
	private ArrayList<Card> _p3cards;
	private ArrayList<Card> _p4cards;
	private ArrayList<Card> _p5cards;
	private ArrayList<Card> _p6cards;
	
	private WeaponGuessEventHandler wg;
	private WeaponGuessEventHandler wg1;
	private WeaponGuessEventHandler wg2;
	private WeaponGuessEventHandler wg3;
	private WeaponGuessEventHandler wg4;
	private WeaponGuessEventHandler wg5;
	private WeaponGuessEventHandler wg6;

	private JButton _sb;
	private JButton _sb1;
	private JButton _sb2;
	private JButton _sb3;
	private JButton _sb4;
	private JButton _sb5;	
	private SuspectGuessEventHandler _sg;
	private SuspectGuessEventHandler _sg1;
	private SuspectGuessEventHandler _sg2;
	private SuspectGuessEventHandler _sg3;
	private SuspectGuessEventHandler _sg4;
	private SuspectGuessEventHandler _sg5;

	private JFrame _finalAccWindow;
	
	    public void run() {
	    	_run = 0;
	    	_alRun = 0;
	    	boolean reallySuperPointless = true;
	    	boolean hannahIsMakingMeDoThis = true;
	        _window = new JFrame("Who Killed Mr. Body-do");
		    _p1Label = new JLabel("Ms Scarlet");
		    _p2Label = new JLabel("Prof Plum");
		    _p3Label = new JLabel("Mr. Green");
		    _p4Label = new JLabel("Mrs. White");
		    _p5Label = new JLabel("Mrs. Peacock");
		    _p6Label = new JLabel("Col Mustard");
		    _playerTurn = 6;
			_menuPanel = new JPanel(new GridLayout(7,1));
			
			_movesLeftPanel = new JPanel();
			_menuPanel.add(_movesLeftPanel);
			
			_up = new JButton ("up");
			_down = new JButton ("down");
			_left = new JButton ("left");
			_right = new JButton ("right");
			_finalAcc = new JButton("Final Accusation");
			_direction = new JPanel(new GridLayout(3,3));
			_suspectsPanel = new JPanel(new GridLayout(6,1));
			
			_weaponsPanel = new JPanel(new GridLayout(7,1));
			_suspectsPanel = new JPanel(new GridLayout(6,1));
			
			_die = new JButton("Dice");
			_displayCards = new JButton("Display Cards");
			Guess = new JButton("Guess");
	        _window.setVisible(true);
	        _window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	        _window.getContentPane().setLayout(new FlowLayout());
	        _window.setSize(new Dimension(900, 900));
			_die.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent e){
					if (_player1.getMovesLeft() == 0 && _player2.getMovesLeft() == 0 && _player3.getMovesLeft() == 0&& _player4.getMovesLeft() == 0&& _player5.getMovesLeft() == 0&& _player6.getMovesLeft() == 0){	
						
						ArrayList<Boolean> list = new ArrayList<Boolean>();
						list.add(true);
						list.add(_player1.kneeCheckup());
						list.add(_player2.kneeCheckup());
						list.add(_player3.kneeCheckup());
						list.add(_player4.kneeCheckup());
						list.add(_player5.kneeCheckup());
						list.add(_player6.kneeCheckup());
						
						if (_playerTurn == 6){_playerTurn = 1;}
						else{_playerTurn++;}
						
						while(list.get(_playerTurn)){
							if (_playerTurn == 6){_playerTurn = 1;}
							else{_playerTurn++;}
						}
						
						Random r = new Random();
						int steps = r.nextInt(6)+1;
						String s = Integer.toString(steps);
						_die.setText("Dice");
						if (_playerTurn == 1){
							_player1.setOneGuessLeft(true);
							_player1.setUnfinishedBusiness(false);
							_player1.setMovesLeft(steps);
							pTurn(_player1, steps);
						}
						else if (_playerTurn == 2){
							_player2.setOneGuessLeft(true);
							_player2.setUnfinishedBusiness(false);
							_player2.setMovesLeft(steps);
							pTurn(_player2, steps);
						}
						else if (_playerTurn == 3){
							_player3.setOneGuessLeft(true);
							_player3.setUnfinishedBusiness(false);
							_player3.setMovesLeft(steps);
							pTurn(_player3, steps);
						}
						else if (_playerTurn == 4){
							_player4.setOneGuessLeft(true);
							_player4.setUnfinishedBusiness(false);
							_player4.setMovesLeft(steps);
							pTurn(_player4, steps);
						}
						else if (_playerTurn == 5){
							_player5.setOneGuessLeft(true);
							_player5.setUnfinishedBusiness(false);
							_player5.setMovesLeft(steps);
							pTurn(_player5, steps);
						}
						else if (_playerTurn == 6){
							_player6.setOneGuessLeft(true);
							_player6.setUnfinishedBusiness(false);
							_player6.setMovesLeft(steps);
							pTurn(_player6, steps);
						}
						_player1.setFrozen(false);
						_player2.setFrozen(false);
						_player3.setFrozen(false);
						_player4.setFrozen(false);
						_player5.setFrozen(false);
						_player6.setFrozen(false);
					}
					else {
						if (_playerTurn == 1){_player1.setUnfinishedBusiness(true);}
						if (_playerTurn == 2){_player2.setUnfinishedBusiness(true);}
						if (_playerTurn == 3){_player3.setUnfinishedBusiness(true);}
						if (_playerTurn == 4){_player4.setUnfinishedBusiness(true);}
						if (_playerTurn == 5){_player5.setUnfinishedBusiness(true);}
						if (_playerTurn == 6){_player6.setUnfinishedBusiness(true);}
					}
					_player1.setJumpingTheGun(false);
					_player2.setJumpingTheGun(false);
					_player3.setJumpingTheGun(false);
					_player4.setJumpingTheGun(false);
					_player5.setJumpingTheGun(false);
					_player6.setJumpingTheGun(false);
					
					_player1.setHeGone(false);
					_player2.setHeGone(false);
					_player3.setHeGone(false);
					_player4.setHeGone(false);
					_player5.setHeGone(false);
					_player6.setHeGone(false);
				}
			});

			
			cardCreate();
			solutionCards();
			startLocation();
			updateGraphics();
			_player1.addObserver(this);
			_player2.addObserver(this);
			_player3.addObserver(this);
			_player4.addObserver(this);
			_player5.addObserver(this);
			_player6.addObserver(this);
			

			_displayCards.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e){
				_alRun++;
				if (_alRun > 1){_displayCardsWindow.getContentPane().removeAll();}
				_displayCardsWindow = new JFrame();
				 _displayCardsWindow.setSize(new Dimension(20, 200));
				JPanel cardPanel = new JPanel();
				cardPanel.setLayout(new GridLayout(8,1));
				
				if (_playerTurn == 1){
					for (int i = 0; i < _p1cards.size(); i++){
						JLabel c = new JLabel(_p1cards.get(i).getName());
						c.setFont(new Font("Courier", Font.PLAIN, 14));
						cardPanel.add(c);
					}
					_displayCardsWindow.add(cardPanel);
					_displayCardsWindow.pack();
					_displayCardsWindow.setAlwaysOnTop(true);
					_displayCardsWindow.setVisible(true);
					
				}
				else if (_playerTurn == 2){
					for (int i = 0; i < _p2cards.size(); i++){
						JLabel c = new JLabel(_p2cards.get(i).getName());
						c.setFont(new Font("Courier", Font.PLAIN, 14));
						cardPanel.add(c);
					}
					_displayCardsWindow.add(cardPanel);
					_displayCardsWindow.pack();
					_displayCardsWindow.setAlwaysOnTop(true);
					_displayCardsWindow.setVisible(true);
				}
				else if (_playerTurn == 3){
					for (int i = 0; i < _p3cards.size(); i++){
						JLabel c = new JLabel(_p3cards.get(i).getName());
						c.setFont(new Font("Courier", Font.PLAIN, 14));
						cardPanel.add(c);
					}
					_displayCardsWindow.add(cardPanel);
					_displayCardsWindow.pack();
					_displayCardsWindow.setAlwaysOnTop(true);
					_displayCardsWindow.setVisible(true);
				}
				else if (_playerTurn == 4){
					for (int i = 0; i < _p4cards.size(); i++){
						JLabel c = new JLabel(_p4cards.get(i).getName());
						c.setFont(new Font("Courier", Font.PLAIN, 14));
						cardPanel.add(c);
					}
					_displayCardsWindow.add(cardPanel);
					_displayCardsWindow.pack();
					_displayCardsWindow.setAlwaysOnTop(true);
					_displayCardsWindow.setVisible(true);
				}
				else if (_playerTurn == 5){
					for (int i = 0; i < _p5cards.size(); i++){
						JLabel c = new JLabel(_p5cards.get(i).getName());
						c.setFont(new Font("Courier", Font.PLAIN, 14));
						cardPanel.add(c);
					}
					_displayCardsWindow.add(cardPanel);
					_displayCardsWindow.pack();
					_displayCardsWindow.setAlwaysOnTop(true);
					_displayCardsWindow.setVisible(true);
				}
				else if (_playerTurn == 6){
					for (int i = 0; i < _p6cards.size(); i++){
						JLabel c = new JLabel(_p6cards.get(i).getName());
						c.setFont(new Font("Courier", Font.PLAIN, 14));
						cardPanel.add(c);
					}
					_displayCardsWindow.add(cardPanel);
					_displayCardsWindow.pack();
					_displayCardsWindow.setAlwaysOnTop(true);
					_displayCardsWindow.setVisible(true);
				}
				
				
		}
	});
			_finalAcc.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e){
					
					Player currentPlayer;
					if (_playerTurn == 1){currentPlayer = _player1;}
					else if (_playerTurn == 2) {currentPlayer = _player2;}
					else if (_playerTurn == 3){currentPlayer = _player3;}
					else if (_playerTurn == 4){currentPlayer = _player4;}
					else if (_playerTurn == 5){currentPlayer = _player5;}
					else {currentPlayer = _player6;}
					
					currentPlayer.setMovesLeft(0);
					if (currentPlayer.kneeCheckup()){
						currentPlayer.setHeGone(true);
						update();
					}
					else if (_board[currentPlayer.getr1()][currentPlayer.getc1()] < -50){
						_finalAccWindow = new JFrame();
						 _finalAccWindow.setSize(new Dimension(900, 900));
						 currentPlayer.setJumpingTheGun(false);
						bizarroUpdateGraphics(currentPlayer, true);
						currentPlayer.setHeGone(false);
					}
					else{
						currentPlayer.setJumpingTheGun(true);
						currentPlayer.setHeGone(false);
					}
			}
		});
			_textPanel = new JPanel(new GridLayout(7,2));
	    }
	    
	    public void bizarroUpdateGraphics(Player currentPlayer, Boolean fromActionListener){
	    	if (fromActionListener == false){_finalAccWindow.getContentPane().removeAll();}
	    	JPanel basePanel = new JPanel();
	    	basePanel.setLayout(new GridLayout(1,3));
			JPanel roomPanel = new JPanel();
			roomPanel.setLayout(new GridLayout(8,1));
			JPanel weaponPanel = new JPanel();
			weaponPanel.setLayout(new GridLayout(8,1));
			JPanel suspectPanel = new JPanel();
			suspectPanel.setLayout(new GridLayout(8,1));
			 
								
			JButton sb = new JButton("Miss Scarlet");
			SuspectFinalEventHandler _sg = new SuspectFinalEventHandler(s_missScarlet, currentPlayer);
			sb.addActionListener(_sg);
			if (currentPlayer.getFinalSuspect() != null && s_missScarlet.getName().equals(currentPlayer.getFinalSuspect().getName())){sb.setBackground(Color.lightGray);}
			else{sb.setBackground(null);}
			suspectPanel.add(sb);
			
			JButton _sb1 = new JButton("Colonel Mustard");
			SuspectFinalEventHandler _sg1 = new SuspectFinalEventHandler(s_colonelMustard, currentPlayer);
			_sb1.addActionListener(_sg1);
			if (currentPlayer.getFinalSuspect() != null && s_colonelMustard.getName().equals(currentPlayer.getFinalSuspect().getName())){_sb1.setBackground(Color.lightGray);}
			else{_sb1.setBackground(null);}
			suspectPanel.add(_sb1);
			
			JButton _sb2 = new JButton("Mrs. White");
			SuspectFinalEventHandler _sg2 = new SuspectFinalEventHandler(s_mrsWhite, currentPlayer);
			if (currentPlayer.getFinalSuspect() != null && s_mrsWhite.getName().equals(currentPlayer.getFinalSuspect().getName())){_sb2.setBackground(Color.lightGray);}
			else{_sb2.setBackground(null);}
			_sb2.addActionListener(_sg2);
			suspectPanel.add(_sb2);
			
			JButton _sb3 = new JButton("Mr. Green");
			SuspectFinalEventHandler _sg3 = new SuspectFinalEventHandler(s_mrGreen, currentPlayer);
			_sb3.addActionListener(_sg3);
			if (currentPlayer.getFinalSuspect() != null && s_mrGreen.getName().equals(currentPlayer.getFinalSuspect().getName())){_sb3.setBackground(Color.lightGray);}
			else{_sb3.setBackground(null);}
			suspectPanel.add(_sb3);
			
			JButton _sb4 = new JButton("Mrs. Peacock");
			SuspectFinalEventHandler _sg4 = new SuspectFinalEventHandler(s_mrsPeacock, currentPlayer);
			_sb4.addActionListener(_sg4);
			if (currentPlayer.getFinalSuspect() != null && s_mrsPeacock.getName().equals(currentPlayer.getFinalSuspect().getName())){_sb4.setBackground(Color.lightGray);}
			else{_sb4.setBackground(null);}
			suspectPanel.add(_sb4);
			
			JButton _sb5 = new JButton("Professor Plum");
			SuspectFinalEventHandler _sg5 = new SuspectFinalEventHandler(s_professorPlum, currentPlayer);
			_sb5.addActionListener(_sg5);
			if (currentPlayer.getFinalSuspect() != null && s_missScarlet.getName().equals(currentPlayer.getFinalSuspect().getName())){_sb5.setBackground(Color.lightGray);}
			else{_sb5.setBackground(null);}
			suspectPanel.add(_sb5);
			
			
			JButton wb = new JButton("candlestick");
			WeaponFinalEventHandler wg = new WeaponFinalEventHandler(w_candleStick, currentPlayer);
			 wb.addActionListener(wg);
			 if (currentPlayer.getFinalWeapon() != null && w_candleStick.getName().equals(currentPlayer.getFinalWeapon().getName())){wb.setBackground(Color.lightGray);}
			 else{wb.setBackground(null);}
			weaponPanel.add(wb);			
			
			JButton wb1 = new JButton("rope");
			WeaponFinalEventHandler wg1 = new WeaponFinalEventHandler(w_rope, currentPlayer);
			 wb1.addActionListener(wg1);
			 if (currentPlayer.getFinalWeapon() != null && w_rope.getName().equals(currentPlayer.getFinalWeapon().getName())){wb1.setBackground(Color.lightGray);}
			 else{wb1.setBackground(null);}
			 weaponPanel.add(wb1);
			
			JButton wb2 = new JButton("wrench");
			WeaponFinalEventHandler wg2 = new WeaponFinalEventHandler(w_wrench, currentPlayer);
			 wb2.addActionListener(wg2);
			 if (currentPlayer.getFinalWeapon() != null && w_wrench.getName().equals(currentPlayer.getFinalWeapon().getName())){wb2.setBackground(Color.lightGray);}
			 else{wb2.setBackground(null);}
			 weaponPanel.add(wb2);
			
			JButton wb3 = new JButton("lead pipe");
			WeaponFinalEventHandler wg3 = new WeaponFinalEventHandler(w_leadPipe, currentPlayer);
			 wb3.addActionListener(wg3);
			 if (currentPlayer.getFinalWeapon() != null && w_leadPipe.getName().equals(currentPlayer.getFinalWeapon().getName())){wb3.setBackground(Color.lightGray);}
			 else{wb3.setBackground(null);}
			 weaponPanel.add(wb3);
			
			JButton wb4 = new JButton("revolver");
			WeaponFinalEventHandler wg4 = new WeaponFinalEventHandler(w_revolver, currentPlayer);
			 wb4.addActionListener(wg4);
			 if (currentPlayer.getFinalWeapon() != null && w_revolver.getName().equals(currentPlayer.getFinalWeapon().getName())){wb4.setBackground(Color.lightGray);}
			 else{wb4.setBackground(null);}
			 weaponPanel.add(wb4);
			
			JButton wb5 = new JButton("knife");
			WeaponFinalEventHandler wg5 = new WeaponFinalEventHandler(w_knife, currentPlayer);
			 wb5.addActionListener(wg5);
			 if (currentPlayer.getFinalWeapon() != null && w_knife.getName().equals(currentPlayer.getFinalWeapon().getName())){wb5.setBackground(Color.lightGray);}
			 else{wb5.setBackground(null);}
			 weaponPanel.add(wb5);
			
			JButton wb6 = new JButton("poison dart frog");
			WeaponFinalEventHandler wg6 = new WeaponFinalEventHandler(w_poisonDartFrog, currentPlayer);
			 wb6.addActionListener(wg6);
			 if (currentPlayer.getFinalWeapon() != null && w_poisonDartFrog.getName().equals(currentPlayer.getFinalWeapon().getName())){wb6.setBackground(Color.lightGray);}
			 else{wb6.setBackground(null);}
			 weaponPanel.add(wb6);
			
			
			JButton rb = new JButton("lounge");
			 RoomFinalEventHandler rg = new RoomFinalEventHandler(r_lounge, currentPlayer);
			 rb.addActionListener(rg);
			 if (currentPlayer.getFinalRoom() != null && r_lounge.getName().equals(currentPlayer.getFinalRoom().getName())){rb.setBackground(Color.lightGray);}
			 else{rb.setBackground(null);}
			roomPanel.add(rb);			
			
			JButton rb1 = new JButton("study");
			RoomFinalEventHandler rg1 = new RoomFinalEventHandler(r_study, currentPlayer);
			 rb1.addActionListener(rg1);
			 if (currentPlayer.getFinalRoom() != null && r_study.getName().equals(currentPlayer.getFinalRoom().getName())){rb1.setBackground(Color.lightGray);}
			 else{rb1.setBackground(null);}
			roomPanel.add(rb1);
			
			JButton rb2 = new JButton("ballroom");
			RoomFinalEventHandler rg2 = new RoomFinalEventHandler(r_ballroom, currentPlayer);
			 rb2.addActionListener(rg2);
			 if (currentPlayer.getFinalRoom() != null && r_ballroom.getName().equals(currentPlayer.getFinalRoom().getName())){rb2.setBackground(Color.lightGray);}
			 else{rb2.setBackground(null);}
			roomPanel.add(rb2);
			
			JButton rb3 = new JButton("kitchen");
			RoomFinalEventHandler rg3 = new RoomFinalEventHandler(r_kitchen, currentPlayer);
			 rb3.addActionListener(rg3);
			 if (currentPlayer.getFinalRoom() != null && r_kitchen.getName().equals(currentPlayer.getFinalRoom().getName())){rb3.setBackground(Color.lightGray);}
			 else{rb3.setBackground(null);}
			roomPanel.add(rb3);
			
			JButton rb4 = new JButton("conservatory");
			RoomFinalEventHandler rg4 = new RoomFinalEventHandler(r_conservatory, currentPlayer);
			 rb4.addActionListener(rg4);
			 if (currentPlayer.getFinalRoom() != null && r_conservatory.getName().equals(currentPlayer.getFinalRoom().getName())){rb4.setBackground(Color.lightGray);}
			 else{rb4.setBackground(null);}
			roomPanel.add(rb4);
			
			JButton rb5 = new JButton("library");
			RoomFinalEventHandler rg5 = new RoomFinalEventHandler(r_library, currentPlayer);
			 rb5.addActionListener(rg5);
			 if (currentPlayer.getFinalRoom() != null && r_library.getName().equals(currentPlayer.getFinalRoom().getName())){rb5.setBackground(Color.lightGray);}
			 else{rb5.setBackground(null);}
			roomPanel.add(rb5);
			
			JButton rb6 = new JButton("dining room");
			RoomFinalEventHandler rg6 = new RoomFinalEventHandler(r_diningRoom, currentPlayer);
			 rb6.addActionListener(rg6);
			 if (currentPlayer.getFinalRoom() != null && r_diningRoom.getName().equals(currentPlayer.getFinalRoom().getName())){rb6.setBackground(Color.lightGray);}
			 else{rb6.setBackground(null);}
			roomPanel.add(rb6);
			
			JButton rb7 = new JButton("billiard room");
			RoomFinalEventHandler rg7 = new RoomFinalEventHandler(r_billiardRoom, currentPlayer);
			 rb7.addActionListener(rg7);
			 if (currentPlayer.getFinalRoom() != null && r_billiardRoom.getName().equals(currentPlayer.getFinalRoom().getName())){rb7.setBackground(Color.lightGray);}
			 else{rb7.setBackground(null);}
			roomPanel.add(rb7);
			
			basePanel.add(roomPanel);
			basePanel.add(weaponPanel);
			basePanel.add(suspectPanel);
			_finalAccWindow.add(basePanel);
			_finalAccWindow.pack();
			_finalAccWindow.setAlwaysOnTop(true);
			_finalAccWindow.setVisible(true);
	    }
	    
	    public Boolean compareSolution(Player p){
	    	if (p.getFinalRoom().getName().equals(r_solution.getName()) && p.getFinalSuspect().getName().equals(s_solution.getName()) && p.getFinalWeapon().getName().equals(w_solution.getName())){
	    		return true;
	    	}
	    	else {
	    		return false;
	    	}
	    }
	    
	    public void pTurn(Player p, int steps){
	    	int index = p.getIndex();
	    	Font current = new Font("Serif", Font.BOLD,18);
	    	Font notCurrent = new Font("Serif", Font.PLAIN, 14);
	    	if (index == 1){
	    		_p1Label.setFont(current);
	    		_p1Label.setOpaque(true);
	    		_p2Label.setFont(notCurrent);
	    		_p2Label.setOpaque(false);
	    		_p3Label.setFont(notCurrent);
	    		_p3Label.setOpaque(false);
	    		_p4Label.setFont(notCurrent);
	    		_p4Label.setOpaque(false);
	    		_p5Label.setFont(notCurrent);
	    		_p5Label.setOpaque(false);
	    		_p6Label.setFont(notCurrent);
	    		_p6Label.setOpaque(false);
	    		}
	    	if (index == 2){
	    		_p1Label.setFont(notCurrent);
	    		_p1Label.setOpaque(false);
	    		_p2Label.setFont(current);
	    		_p2Label.setOpaque(true);
	    		_p3Label.setFont(notCurrent);
	    		_p3Label.setOpaque(false);
	    		_p4Label.setFont(notCurrent);
	    		_p4Label.setOpaque(false);
	    		_p5Label.setFont(notCurrent);
	    		_p5Label.setOpaque(false);
	    		_p6Label.setFont(notCurrent);
	    		_p6Label.setOpaque(false);
	    		}	    	
	    	if (index == 3){
	    		_p1Label.setFont(notCurrent);
	    		_p1Label.setOpaque(false);
	    		_p2Label.setFont(notCurrent);
	    		_p2Label.setOpaque(false);
	    		_p3Label.setFont(current);
	    		_p3Label.setOpaque(true);
	    		_p4Label.setFont(notCurrent);
	    		_p4Label.setOpaque(false);
	    		_p5Label.setFont(notCurrent);
	    		_p5Label.setOpaque(false);
	    		_p6Label.setFont(notCurrent);
	    		_p6Label.setOpaque(false);
	    	}
	    	if (index == 4){
	    		_p1Label.setFont(notCurrent);
	    		_p1Label.setOpaque(false);
	    		_p2Label.setFont(notCurrent);
	    		_p2Label.setOpaque(false);
	    		_p3Label.setFont(notCurrent);
	    		_p3Label.setOpaque(false);
	    		_p4Label.setFont(current);
	    		_p4Label.setOpaque(true);
	    		_p5Label.setFont(notCurrent);
	    		_p5Label.setOpaque(false);
	    		_p6Label.setFont(notCurrent);
	    		_p6Label.setOpaque(false);
	    	}
	    	if (index == 5){
	    		_p1Label.setFont(notCurrent);
	    		_p1Label.setOpaque(false);
	    		_p2Label.setFont(notCurrent);
	    		_p2Label.setOpaque(false);
	    		_p3Label.setFont(notCurrent);
	    		_p3Label.setOpaque(false);
	    		_p4Label.setFont(notCurrent);
	    		_p4Label.setOpaque(false);
	    		_p5Label.setFont(current);
	    		_p5Label.setOpaque(true);
	    		_p6Label.setFont(notCurrent);
	    		_p6Label.setOpaque(false);
	    	}
	    	if (index == 6){
	    		_p1Label.setFont(notCurrent);
	    		_p1Label.setOpaque(false);
	    		_p2Label.setFont(notCurrent);
	    		_p2Label.setOpaque(false);
	    		_p3Label.setFont(notCurrent);
	    		_p3Label.setOpaque(false);
	    		_p4Label.setFont(notCurrent);
	    		_p4Label.setOpaque(false);
	    		_p5Label.setFont(notCurrent);
	    		_p5Label.setOpaque(false);
	    		_p6Label.setFont(current);
	    		_p6Label.setOpaque(true);
	    	}
	    	updateGraphics();
	    }
	    
	    public void updateGraphics(){
			Player currentPlayer;
			if (_playerTurn == 1){currentPlayer = _player1;}
			else if (_playerTurn == 2) {currentPlayer = _player2;}
			else if (_playerTurn == 3){currentPlayer = _player3;}
			else if (_playerTurn == 4){currentPlayer = _player4;}
			else if (_playerTurn == 5){currentPlayer = _player5;}
			else {currentPlayer = _player6;}
			
	    	_run++; //keeps track of how many times graphics were updated
	    	_window.getContentPane().removeAll();
	    	_direction.removeAll();
	    	
	    	_menuPanel.removeAll();
	    	_menuPanel.add(_movesLeftPanel);
	    	_menuPanel.add(_die);
			_menuPanel.add(_displayCards);
			_menuPanel.add(Guess);
			_menuPanel.add(_finalAcc);
			_direction.add(new JPanel());
			_direction.add(_up);
			_direction.add(new JPanel());
			_direction.add(_left);
			_direction.add(new JPanel());
			_direction.add(_right);
			_direction.add(new JPanel());
			_direction.add(_down);
			_window.add(_direction);
			if (currentPlayer.getPlayerGuessing()){
				Guess.setBackground(Color.lightGray);
			}else{
				Guess.setBackground(null);
			}
			_weaponsPanel.removeAll();
			
			if (_run > 1){ // removes action listeners so that only one is there at a time
				_up.removeActionListener(_ueh);
				_down.removeActionListener(_deh);
				_right.removeActionListener(_reh);
				_left.removeActionListener(_leh);
			}
			if (_run > 1){ // removes action listeners so that only one is there at a time
				wb.removeActionListener(wg);
				wb1.removeActionListener(wg1);
				wb2.removeActionListener(wg2);
				wb3.removeActionListener(wg3);
				wb4.removeActionListener(wg4);
				wb5.removeActionListener(wg5);
				wb6.removeActionListener(wg6);

			}
			_suspectsPanel.removeAll();
			if (_run > 1){
				_sb.removeActionListener(_sg);
				_sb1.removeActionListener(_sg1);
				_sb2.removeActionListener(_sg2);
				_sb3.removeActionListener(_sg3);
				_sb4.removeActionListener(_sg4);
				_sb5.removeActionListener(_sg5);
				
				Guess.removeActionListener(_guessHandler);
			}
			
			_ueh = new UpButtonEventHandler(currentPlayer);
			_up.addActionListener(_ueh);
			_deh = new DownButtonEventHandler(currentPlayer);
			_down.addActionListener(_deh);
			_leh = new LeftButtonEventHandler(currentPlayer);
			_left.addActionListener(_leh);
			_reh = new RightButtonEventHandler(currentPlayer);
			_right.addActionListener(_reh);
			
			_guessHandler = new GuessEventHandler(currentPlayer);
			Guess.addActionListener(_guessHandler);
			
			_boardPanel = new JPanel();
			GridLayout layout = new GridLayout(8,8);
			_boardPanel.setLayout(layout); 
			_movesLeftPanel.removeAll();
			JLabel movesLeftLabel = new JLabel("Moves Left: "+ Integer.toString(currentPlayer.getMovesLeft()));
			movesLeftLabel.setFont(new Font("Courier", Font.BOLD,20));
			movesLeftLabel.setForeground(Color.RED);
			_movesLeftPanel.add(movesLeftLabel);
			gridInitialization();
			
			 for (int r = 0; r < 8; r++) {
				  for (int c= 0; c < 8; c++) {
				    _floor = new JPanel();
				    JLabel label1 = new JLabel("Secret Hall 1");
				    JLabel label2 = new JLabel("Secret Hall 2");
				    if (_board[r][c] == 1 || _board[r][c] == 1.1){_floor.setBackground(Color.red);}
				    if (_board[r][c] == 2){_floor.setBackground(Color.magenta);}
				    if (_board[r][c] == 3 || _board[r][c] == 3.1){_floor.setBackground(Color.yellow);}
				    if (_board[r][c] == 4){_floor.setBackground(Color.green);}
				    if (_board[r][c] == 5){_floor.setBackground(Color.blue);}
				    if (_board[r][c] == 6){_floor.setBackground(Color.orange);}
				    if (_board[r][c] == 7 || _board[r][c] == 7.1){_floor.setBackground(Color.cyan);}
				    if (_board[r][c] == 8){_floor.setBackground(Color.BLUE);}
				    if (_board[r][c] == 9 || _board[r][c] == 9.1){_floor.setBackground(Color.pink);}
				    if (_board[r][c] < 0){_floor.setBackground(Color.lightGray);}
				    if (_board[r][c] == 0 || _board[r][c] >= 10){_floor.setBackground(Color.white);}
				    if (_board[r][c] == 1.1 || _board[r][c] == 9.1){_floor.add(label1); label1.setFont(new Font("Times New Roman", Font.ITALIC, 16));}
				    if (_board[r][c] == 3.1 || _board[r][c] == 7.1){_floor.add(label2); label2.setFont(new Font("Times New Roman", Font.ITALIC, 16));}
					_floor.setPreferredSize(new Dimension(100, 100)); 
					_floor.setBorder(BorderFactory.createEtchedBorder());
					_roomLabel = new JLabel();
					if (r == 0 && c == 1){_roomLabel.setText("Conservatory"); _roomLabel.setFont(new Font("Serif", Font.BOLD, 15));}
					else if (r == 0 && c == 4){_roomLabel.setText("Billiard Room"); _roomLabel.setFont(new Font("Serif", Font.BOLD, 15));}
					else if (r == 0 && c == 6){_roomLabel.setText("Kitchen"); _roomLabel.setFont(new Font("Serif", Font.BOLD, 18));}
					else if (r == 3 && c == 0){_roomLabel.setText("Library"); _roomLabel.setFont(new Font("Serif", Font.BOLD, 18));}
					else if (r == 3 && c == 7){_roomLabel.setText("Dining Room"); _roomLabel.setFont(new Font("Serif", Font.BOLD, 15));}
					else if (r == 6 && c == 0){_roomLabel.setText("Lounge"); _roomLabel.setFont(new Font("Serif", Font.BOLD, 18));}
					else if (r == 6 && c == 3){_roomLabel.setText("Ballroom"); _roomLabel.setFont(new Font("Serif", Font.BOLD, 18));}
					else if (r == 6 && c == 7){_roomLabel.setText("Study"); _roomLabel.setFont(new Font("Serif", Font.BOLD, 18));}
					_floor.add(_roomLabel);
					
					JLabel warning = new JLabel("Illegal!");
					warning.setForeground(Color.RED);
					
					JLabel yaFroze = new JLabel("No Moves!");
					yaFroze.setForeground(Color.RED);
					if (_board[r][c] == 1 || _board[r][c] == 1.1 || _board[r][c] == 2 || _board[r][c] == 8){yaFroze.setForeground(Color.WHITE);}
					
					JLabel hallwayGuessin = new JLabel("Get a Room!");
					hallwayGuessin.setForeground(Color.RED);
					
					JLabel unfinishedBusiness = new JLabel("Finish Your Turn!");
					unfinishedBusiness.setForeground(Color.RED);
					if (_board[r][c] == 1 || _board[r][c] == 1.1 || _board[r][c] == 2 || _board[r][c] == 8){unfinishedBusiness.setForeground(Color.WHITE);}
					
					JLabel gunJumping = new JLabel("Go to Middle!");
					gunJumping.setForeground(Color.RED);
					if (_board[r][c] == 1 || _board[r][c] == 1.1 || _board[r][c] == 2 || _board[r][c] == 8){gunJumping.setForeground(Color.WHITE);}
					
					JLabel sheGone = new JLabel("Ya done for");
					sheGone.setForeground(Color.RED);
					
					if (_player1.getc1() == c && _player1.getr1() == r){
						if (_player1.getJWalk()){_floor.add(warning);}
						if (_player1.getFrozen()){_floor.add(yaFroze);}
						if (_player1.getHallwayGuessin()){_floor.add(hallwayGuessin);}
						if (_player1.getJumpingTheGun()){_floor.add(gunJumping);}
						if (_player1.getHeGone()){_floor.add(sheGone);}
						if (_player1.getUnfinishedBusiness()){_floor.add(unfinishedBusiness);}
						_floor.add(_p1Label);
					}
					if (_player2.getc1() == c && _player2.getr1() == r){
						if (_player2.getJWalk()){_floor.add(warning);}
						if (_player2.getFrozen()){_floor.add(yaFroze);}
						if (_player2.getHallwayGuessin()){_floor.add(hallwayGuessin);}
						if (_player2.getJumpingTheGun()){_floor.add(gunJumping);}
						if (_player2.getHeGone()){_floor.add(sheGone);}
						if (_player2.getUnfinishedBusiness()){_floor.add(unfinishedBusiness);}
						_floor.add(_p2Label);
					}
					if (_player3.getc1() == c && _player3.getr1() == r){
						if (_player3.getJWalk()){_floor.add(warning);}
						if (_player3.getFrozen()){_floor.add(yaFroze);}
						if (_player3.getHallwayGuessin()){_floor.add(hallwayGuessin);}
						if (_player3.getJumpingTheGun()){_floor.add(gunJumping);}
						if (_player3.getHeGone()){_floor.add(sheGone);}
						if (_player3.getUnfinishedBusiness()){_floor.add(unfinishedBusiness);}
						_floor.add(_p3Label);
					}
					if (_player4.getc1() == c && _player4.getr1() == r){
						if (_player4.getJWalk()){_floor.add(warning);}
						if (_player4.getFrozen()){_floor.add(yaFroze);}
						if (_player4.getHallwayGuessin()){_floor.add(hallwayGuessin);}
						if (_player4.getJumpingTheGun()){_floor.add(gunJumping);}
						if (_player4.getHeGone()){_floor.add(sheGone);}
						if (_player4.getUnfinishedBusiness()){_floor.add(unfinishedBusiness);}
						_floor.add(_p4Label);
					}
					if (_player5.getc1() == c && _player5.getr1() == r){
						if (_player5.getJWalk()){_floor.add(warning);}
						if (_player5.getFrozen()){_floor.add(yaFroze);}
						if (_player5.getHallwayGuessin()){_floor.add(hallwayGuessin);}
						if (_player5.getJumpingTheGun()){_floor.add(gunJumping);}
						if (_player5.getHeGone()){_floor.add(sheGone);}
						if (_player5.getUnfinishedBusiness()){_floor.add(unfinishedBusiness);}
						_floor.add(_p5Label);
					}
					if (_player6.getc1() == c && _player6.getr1() == r){
						if (_player6.getJWalk()){_floor.add(warning);}
						if (_player6.getFrozen()){_floor.add(yaFroze);}
						if (_player6.getHallwayGuessin()){_floor.add(hallwayGuessin);}
						if (_player6.getJumpingTheGun()){_floor.add(gunJumping);}
						if (_player6.getHeGone()){_floor.add(sheGone);}
						if (_player6.getUnfinishedBusiness()){_floor.add(unfinishedBusiness);}
						_floor.add(_p6Label);
					}

					_boardPanel.add(_floor);	
				   
				}
	    	}
			
			 
			_window.add(_boardPanel,BorderLayout.PAGE_START);
			_window.add(_menuPanel,BorderLayout.PAGE_END);
			_textPanel = new JPanel();
			_window.add(_textPanel);
			
		
			weaponPanelCreate(currentPlayer);
			suspectPanelCreate(currentPlayer);
			
			if (currentPlayer.getFinalRoom() != null && currentPlayer.getFinalSuspect() != null && currentPlayer.getFinalWeapon() != null){
				_finalAccWindow.dispose();
				currentPlayer.setUpdateBizarro(false);
				jennifer = new JFrame();
				jennifer.setSize(700, 200);
				jennifer.setAlwaysOnTop(true);
				jennifer.setVisible(true);
				JPanel deborah = new JPanel();
				JLabel youStink = new JLabel();
				JLabel noYou = new JLabel();
				JLabel noMe = new JLabel();
				if (compareSolution(currentPlayer)){
					jennifer.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
					youStink.setText("You win!!!!!");
					youStink.setFont(new Font("Courier", Font.PLAIN, 50));
					deborah.add(youStink);
				}
				else {
					jennifer.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
					youStink.setText("You lost because you stink." + "\n");
					youStink.setFont(new Font("Courier", Font.PLAIN, 16));
					noYou.setText("You have brought great shame upon your family.");
					noYou.setFont(new Font("Courier", Font.PLAIN, 16));
					noMe.setText("They have disowned you and you are no longer playing the game.");
					noMe.setFont(new Font("Courier", Font.PLAIN, 16));
					deborah.add(youStink);
					deborah.add(noYou);
					currentPlayer.baseballBatToTheKnees(true);
					deborah.add(noMe);
				}
				jennifer.add(deborah);
				
				currentPlayer.setFinalRoom(null);
				currentPlayer.setFinalSuspect(null);
				currentPlayer.setFinalWeapon(null);
				_finalAccWindow.dispose();
			}
			
			if (currentPlayer.getUpdateBizarro()){
				bizarroUpdateGraphics(currentPlayer, false);
				currentPlayer.setUpdateBizarro(false);
			}
			
			if (currentPlayer.getPlayerGuessing() && currentPlayer.getSuspectGuess() != null && currentPlayer.getWeaponGuess() != null){checkGuess(currentPlayer);}
			_window.setVisible(true);
			_window.pack();			
			_window.pack();
			
	    }
	    
	    public void checkGuess(Player cp){
			JFrame revFrame = new JFrame();
	    	int indexOfRevealingPlayer = playerReveal(cp,cp.roomAt(), cp.getWeaponGuess(), cp.getSuspectGuess());
			if (indexOfRevealingPlayer == 2){
				Card reveal = _player2.cardShow(cp.roomAt(), cp.getWeaponGuess(), cp.getSuspectGuess());
				JLabel whoWhat = new JLabel("Professor Plum reveals: " + reveal.getName());
				whoWhat.setFont(new Font ("Courier", Font.PLAIN, 16));
				revFrame.add(whoWhat);
			}
			else if (indexOfRevealingPlayer == 1){
				Card reveal = _player1.cardShow(cp.roomAt(), cp.getWeaponGuess(), cp.getSuspectGuess());
				JLabel whoWhat = new JLabel("Miss Scarlet reveals: " + reveal.getName());
				whoWhat.setFont(new Font ("Courier", Font.PLAIN, 16));
				revFrame.add(whoWhat);
			}
			else if (indexOfRevealingPlayer == 3){
				Card reveal = _player3.cardShow(cp.roomAt(), cp.getWeaponGuess(), cp.getSuspectGuess());
				JLabel whoWhat = new JLabel("Mr. Green reveals: " + reveal.getName());
				whoWhat.setFont(new Font ("Courier", Font.PLAIN, 16));
				revFrame.add(whoWhat);
			}
			else if (indexOfRevealingPlayer == 4){
				Card reveal = _player4.cardShow(cp.roomAt(), cp.getWeaponGuess(), cp.getSuspectGuess());
				JLabel whoWhat = new JLabel("Mrs. White reveals: " + reveal.getName());
				whoWhat.setFont(new Font ("Courier", Font.PLAIN, 16));
				revFrame.add(whoWhat);
			}
			else if (indexOfRevealingPlayer == 5){
				Card reveal = _player5.cardShow(cp.roomAt(), cp.getWeaponGuess(), cp.getSuspectGuess());
				JLabel whoWhat = new JLabel("Mrs. Peacock reveals: " + reveal.getName());
				whoWhat.setFont(new Font ("Courier", Font.PLAIN, 16));
				revFrame.add(whoWhat);
			}
			else if (indexOfRevealingPlayer == 6){
				Card reveal = _player6.cardShow(cp.roomAt(), cp.getWeaponGuess(), cp.getSuspectGuess());
				JLabel whoWhat = new JLabel("Col. Mustard reveals: " + reveal.getName());
				whoWhat.setFont(new Font ("Courier", Font.PLAIN, 16));
				revFrame.add(whoWhat);
			}
			else if (indexOfRevealingPlayer == 0){
				JLabel whoWhat = new JLabel("Nothing to reveal");
				whoWhat.setFont(new Font ("Courier", Font.PLAIN, 16));
				revFrame.add(whoWhat);
			}
			cp.setPlayerGuessing(false);
			cp.suspectGuessing(null);
			cp.WeaponGuessing(null);
			revFrame.setSize(500, 200);
			revFrame.setAlwaysOnTop(true);			
			revFrame.setVisible(true);
	    }
	    
	    public void suspectPanelCreate(Player currentPlayer){
			
			_sb = new JButton("Miss Scarlet");
			_sg = new SuspectGuessEventHandler(s_missScarlet, currentPlayer);
			_sb.addActionListener(_sg);
			if (currentPlayer.getSuspectGuess() != null && s_missScarlet.getName().equals(currentPlayer.getSuspectGuess().getName())){_sb.setBackground(Color.lightGray);}
			else{_sb.setBackground(null);}
			_suspectsPanel.add(_sb);
			
			_sb1 = new JButton("Colonel Mustard");
			_sg1 = new SuspectGuessEventHandler(s_colonelMustard, currentPlayer);
			_sb1.addActionListener(_sg1);
			if (currentPlayer.getSuspectGuess() != null && s_colonelMustard.getName().equals(currentPlayer.getSuspectGuess().getName())){_sb1.setBackground(Color.lightGray);}
			else{_sb1.setBackground(null);}
			_suspectsPanel.add(_sb1);
			
			_sb2 = new JButton("Mrs. White");
			_sg2 = new SuspectGuessEventHandler(s_mrsWhite, currentPlayer);
			if (currentPlayer.getSuspectGuess() != null && s_mrsWhite.getName().equals(currentPlayer.getSuspectGuess().getName())){_sb2.setBackground(Color.lightGray);}
			else{_sb2.setBackground(null);}
			_sb2.addActionListener(_sg2);
			_suspectsPanel.add(_sb2);
			
			_sb3 = new JButton("Mr. Green");
			_sg3 = new SuspectGuessEventHandler(s_mrGreen, currentPlayer);
			_sb3.addActionListener(_sg3);
			if (currentPlayer.getSuspectGuess() != null && s_mrGreen.getName().equals(currentPlayer.getSuspectGuess().getName())){_sb3.setBackground(Color.lightGray);}
			else{_sb3.setBackground(null);}
			_suspectsPanel.add(_sb3);
			
			_sb4 = new JButton("Mrs. Peacock");
			_sg4 = new SuspectGuessEventHandler(s_mrsPeacock, currentPlayer);
			_sb4.addActionListener(_sg4);
			if (currentPlayer.getSuspectGuess() != null && s_mrsPeacock.getName().equals(currentPlayer.getSuspectGuess().getName())){_sb4.setBackground(Color.lightGray);}
			else{_sb4.setBackground(null);}
			_suspectsPanel.add(_sb4);
			
			_sb5 = new JButton("Professor Plum");
			_sg5 = new SuspectGuessEventHandler(s_professorPlum, currentPlayer);
			_sb5.addActionListener(_sg5);
			if (currentPlayer.getSuspectGuess() != null && s_missScarlet.getName().equals(currentPlayer.getSuspectGuess().getName())){_sb5.setBackground(Color.lightGray);}
			else{_sb5.setBackground(null);}
			_suspectsPanel.add(_sb5);
			
			_window.add(_suspectsPanel);
	    }
	    
	    public void weaponPanelCreate(Player currentPlayer){
			wb = new JButton("candlestick");
			 wg = new WeaponGuessEventHandler(w_candleStick, currentPlayer);
			 wb.addActionListener(wg);
			 if (currentPlayer.getWeaponGuess() != null && w_candleStick.getName().equals(currentPlayer.getWeaponGuess().getName())){wb.setBackground(Color.lightGray);}
			 else{wb.setBackground(null);}
			_weaponsPanel.add(wb);			
			
			wb1 = new JButton("rope");
			 wg1 = new WeaponGuessEventHandler(w_rope, currentPlayer);
			 wb1.addActionListener(wg1);
			 if (currentPlayer.getWeaponGuess() != null && w_rope.getName().equals(currentPlayer.getWeaponGuess().getName())){wb1.setBackground(Color.lightGray);}
			 else{wb1.setBackground(null);}
			_weaponsPanel.add(wb1);
			
			wb2 = new JButton("wrench");
			 wg2 = new WeaponGuessEventHandler(w_wrench, currentPlayer);
			 wb2.addActionListener(wg2);
			 if (currentPlayer.getWeaponGuess() != null && w_wrench.getName().equals(currentPlayer.getWeaponGuess().getName())){wb2.setBackground(Color.lightGray);}
			 else{wb2.setBackground(null);}
			_weaponsPanel.add(wb2);
			
			wb3 = new JButton("lead pipe");
			 wg3 = new WeaponGuessEventHandler(w_leadPipe, currentPlayer);
			 wb3.addActionListener(wg3);
			 if (currentPlayer.getWeaponGuess() != null && w_leadPipe.getName().equals(currentPlayer.getWeaponGuess().getName())){wb3.setBackground(Color.lightGray);}
			 else{wb3.setBackground(null);}
			_weaponsPanel.add(wb3);
			
			wb4 = new JButton("revolver");
			 wg4 = new WeaponGuessEventHandler(w_revolver, currentPlayer);
			 wb4.addActionListener(wg4);
			 if (currentPlayer.getWeaponGuess() != null && w_revolver.getName().equals(currentPlayer.getWeaponGuess().getName())){wb4.setBackground(Color.lightGray);}
			 else{wb4.setBackground(null);}
			_weaponsPanel.add(wb4);
			
			wb5 = new JButton("knife");
			 wg5 = new WeaponGuessEventHandler(w_knife, currentPlayer);
			 wb5.addActionListener(wg5);
			 if (currentPlayer.getWeaponGuess() != null && w_knife.getName().equals(currentPlayer.getWeaponGuess().getName())){wb5.setBackground(Color.lightGray);}
			 else{wb5.setBackground(null);}
			_weaponsPanel.add(wb5);
			
			wb6 = new JButton("poison dart frog");
			 wg6 = new WeaponGuessEventHandler(w_poisonDartFrog, currentPlayer);
			 wb6.addActionListener(wg6);
			 if (currentPlayer.getWeaponGuess() != null && w_poisonDartFrog.getName().equals(currentPlayer.getWeaponGuess().getName())){wb6.setBackground(Color.lightGray);}
			 else{wb6.setBackground(null);}
			_weaponsPanel.add(wb6);
			
			
			_window.add(_weaponsPanel);
	    }
	    
	    public int playerReveal(Player playerMakingGuess, Card r, Card w , Card s){	
	    	if (playerMakingGuess.getIndex() == 1){
	    		if (_player2.cardShow(r, w, s) != null){
	    			return 2;
	    		}
	    		else if (_player3.cardShow(r, w, s) != null){
	    			return 3;
	    		}
	    		else if (_player4.cardShow(r, w, s) != null){
	    			return 4;
	    		}
	    		else if (_player5.cardShow(r, w, s) != null){
	    			return 5;
	    		}
	    		else if (_player6.cardShow(r, w, s) != null){
	    			return 6;
	    		}
	    	}
	    	else if (playerMakingGuess.getIndex() == 2){
	    		if (_player3.cardShow(r, w, s) != null){
	    			return 3;
	    		}
	    		else if (_player4.cardShow(r, w, s) != null){
	    			return 4;
	    		}
	    		else if (_player5.cardShow(r, w, s) != null){
	    			return 5;
	    		}
	    		else if (_player6.cardShow(r, w, s) != null){
	    			return 6;
	    		}
	    		else if (_player1.cardShow(r, w, s) != null){
	    			return 1;
	    		}
	    	}
	    	else if (playerMakingGuess.getIndex() == 3){
	    		if (_player4.cardShow(r, w, s) != null){
	    			return 4;
	    		}
	    		else if (_player5.cardShow(r, w, s) != null){
	    			return 5;
	    		}
	    		else if (_player6.cardShow(r, w, s) != null){
	    			return 6;
	    		}
	    		else if (_player1.cardShow(r, w, s) != null){
	    			return 1;
	    		}
	    		else if (_player2.cardShow(r, w, s) != null){
	    			return 2;
	    		}
	    	}
	    	else if (playerMakingGuess.getIndex() == 4){
	    		if (_player5.cardShow(r, w, s) != null){
	    			return 5;
	    		}
	    		else if (_player6.cardShow(r, w, s) != null){
	    			return 6;
	    		}
	    		else if (_player1.cardShow(r, w, s) != null){
	    			return 1;
	    		}
	    		else if (_player2.cardShow(r, w, s) != null){
	    			return 2;
	    		}
	    		else if (_player3.cardShow(r, w, s) != null){
	    			return 3;
	    		}
	    	}
	    	else if (playerMakingGuess.getIndex() == 5){
	    		if (_player6.cardShow(r, w, s) != null){
	    			return 6;
	    		}
	    		else if (_player1.cardShow(r, w, s) != null){
	    			return 1;
	    		}
	    		else if (_player2.cardShow(r, w, s) != null){
	    			return 2;
	    		}
	    		else if (_player3.cardShow(r, w, s) != null){
	    			return 3;
	    		}
	    		else if (_player4.cardShow(r, w, s) != null){
	    			return 4;
	    		}
	    	}
	    	else if (playerMakingGuess.getIndex() == 6){
	    		if (_player1.cardShow(r, w, s) != null){
	    			return 1;
	    		}
	    		else if (_player2.cardShow(r, w, s) != null){
	    			return 2;
	    		}
	    		else if (_player3.cardShow(r, w, s) != null){
	    			return 3;
	    		}
	    		else if (_player4.cardShow(r, w, s) != null){
	    			return 4;
	    		}
	    		else if (_player5.cardShow(r, w, s) != null){
	    			return 5;
	    		}
	    	}
	    	return 0;
	    }
	    
	    public void startLocation(){
	    	_player1 = new Player(_board,0,2, _p1cards);
	    	_player1.setIndex(1);
	    	_player2 = new Player(_board,7,5, _p2cards);
	    	_player2.setIndex(2);
	    	_player3 = new Player(_board,0,5, _p3cards);
	    	_player3.setIndex(3);
	    	_player4 = new Player(_board,7,2, _p4cards);
	    	_player4.setIndex(4);
	    	_player5 = new Player(_board,2,7, _p5cards);
	    	_player5.setIndex(5);
	    	_player6 = new Player(_board,5,0, _p6cards);
	    	_player6.setIndex(6);
	    }
	    
	    public void cardCreate(){
	    	w_solution = null;
	    	r_solution = null;
	    	s_solution = null;
	    	
	    	w_candleStick = new Card("weapon", "candlestick");
	    	w_leadPipe = new Card ("weapon", "lead pipe");
	    	w_rope = new Card ("weapon", "rope");
	    	w_revolver = new Card ("weapon", "revolver");
	    	w_wrench= new Card ("weapon", "wrench");
	    	w_knife = new Card ("weapon", "knife");
	    	w_poisonDartFrog = new Card ("weapon", "poison dart frog");
	    	
	    	r_billiardRoom = new Card("room", "billiard room");
	    	r_conservatory = new Card("room", "conservatory");
	    	r_diningRoom = new Card("room", "dining room");
	    	r_kitchen = new Card("room", "kitchen");
	    	r_ballroom = new Card("room", "ballroom");
	    	r_library = new Card("room", "library");
	    	r_lounge = new Card("room", "lounge");
	    	r_study = new Card("room", "study");
	    	
	    	s_missScarlet = new Card("suspect", "Miss. Scarlet");
	    	s_colonelMustard = new Card("suspect", "Colonel Mustard");
	    	s_mrsWhite = new Card("suspect", "Mrs. White");
	    	s_mrGreen = new Card("suspect", "Mr. Green");
	    	s_mrsPeacock = new Card("suspect", "Mrs. Peacock");
	    	s_professorPlum = new Card("suspect", "Professor Plum");
	    }
	    
	    public void solutionCards(){
	    	_cards = new ArrayList<Card>();
	    	_p1cards = new ArrayList<Card>();
	    	_p2cards = new ArrayList<Card>();
	    	_p3cards = new ArrayList<Card>();
	    	_p4cards = new ArrayList<Card>();
	    	_p5cards = new ArrayList<Card>();
	    	_p6cards = new ArrayList<Card>();

	    	
	    	ArrayList<Card> w_cards = new ArrayList<Card>();
	    	ArrayList<Card> r_cards = new ArrayList<Card>();
	    	ArrayList<Card> s_cards = new ArrayList<Card>();
	    	w_cards.add(w_candleStick);
	    	w_cards.add(w_leadPipe); 
	    	w_cards.add(w_rope);
	    	w_cards.add(w_wrench);
	    	w_cards.add(w_revolver);
	    	w_cards.add(w_knife);
	    	w_cards.add(w_poisonDartFrog);
	    	r_cards.add(r_billiardRoom);
	    	r_cards.add(r_conservatory);
	    	r_cards.add(r_diningRoom);
	    	r_cards.add(r_kitchen);
	    	r_cards.add(r_ballroom);
	    	r_cards.add(r_library);
	    	r_cards.add(r_lounge);
	    	r_cards.add(r_study);
	    	s_cards.add(s_missScarlet);
	    	s_cards.add(s_colonelMustard);
	    	s_cards.add(s_mrsWhite);
	    	s_cards.add(s_mrGreen);
	    	s_cards.add(s_mrsPeacock);
	    	s_cards.add(s_professorPlum);	    	
	    	
	    	Random r = new Random();
	    	int k;
	    	k = r.nextInt(6);
	    	w_solution = w_cards.get(k);
	    	
	    	k = r.nextInt(7);
	    	r_solution = r_cards.get(k);
	    	
	    	k = r.nextInt(5);
	    	s_solution = s_cards.get(k);
	    	
	    	_cards.addAll(w_cards);
	    	_cards.addAll(r_cards);
	    	_cards.addAll(s_cards);
	    	_cards.remove(w_solution);
	    	_cards.remove(r_solution);
	    	_cards.remove(s_solution);
	    	
	    	System.out.println("The solution is:" + "\n" + w_solution.getName()+ "\n" +r_solution.getName()+ "\n" +s_solution.getName());
	    	
	    	int playerTurn = 1;
	    	while(!_cards.isEmpty()){
	    		if (_cards.size() > 1){k = r.nextInt(_cards.size()-1);}
	    		else if (_cards.size() == 1){k = 0;}
	    		
	    		if (playerTurn == 1){_p1cards.add(_cards.get(k));}
	    		else if (playerTurn == 2){_p2cards.add(_cards.get(k));}
	    		else if (playerTurn == 3){_p3cards.add(_cards.get(k));}
	    		else if (playerTurn == 4){_p4cards.add(_cards.get(k));}
	    		else if (playerTurn == 5){_p5cards.add(_cards.get(k));}
	    		else if (playerTurn == 6){_p6cards.add(_cards.get(k));}
	    		_cards.remove(k);
	    		
	    		if (playerTurn == 1){playerTurn = 2;}
	    		else if (playerTurn == 2){playerTurn = 3;}
	    		else if (playerTurn == 3){playerTurn = 4;}
	    		else if (playerTurn == 4){playerTurn = 5;}
	    		else if (playerTurn == 5){playerTurn = 6;}
	    		else if (playerTurn == 6){playerTurn = 1;}
	    	}
	    }
	    
	    public void setPlayer1(Player p1){
	    	_player1 = p1;
	    }
	    
	    public void setPlayer2(Player p2){
	    	_player2 = p2;
	    }
	    
	    public void setPlayer3(Player p3){
	    	_player3 = p3;
	    }
	    
	    public void setPlayer4(Player p4){
	    	_player4 = p4;
	    }
	    
	    public void setPlayer5(Player p5){
	    	_player5 = p5;
	    }
	    
	    public void setPlayer6(Player p6){
	    	_player6 = p6;
	    }
	    
	    public Card getRevealingCard(){
	    	return _revealingCard;
	    }
	    
	    public void gridInitialization(){
		   _board = new double[8][8];
		   for(int i = 0; i < 8; i++){
			   _board[i][2] = 0;
			   _board[i][5] = 0;
			   _board[2][i] = 0;
			   _board[5][i] = 0;
		   }
		   _board[0][0] = 1.1; 
		   _board[0][1] = 1;
		   _board[1][0] = 1;
		   _board[1][1] = -1;
		   _board[0][3] = 2;
		   _board[0][4] = 2;
		   _board[1][4] = 2;
		   _board[1][3] = -2;
		   _board[0][6] = 3;
		   _board[0][7] = 3.1;
		   _board[1][7] = 3;
		   _board[1][6] = -3;
		   _board[3][0] = 4;
		   _board[3][1] = 4;
		   _board[4][0] = 4;
		   _board[4][1] = -4;
		   _board[3][3] = -51;
		   _board[3][4] = -52;
		   _board[4][3] = -53;
		   _board[4][4] = -54;
		   _board[3][7] = 6;
		   _board[4][7] = 6;
		   _board[4][6] = 6;
		   _board[3][6] = -6;
		   _board[6][0] = 7;
		   _board[7][0] = 7.1;
		   _board[7][1] = 7;
		   _board[6][1] = -7;
		   _board[6][3] = 8;
		   _board[7][3] = 8;
		   _board[7][4] = 8;
		   _board[6][4] = -8;
		   _board[6][7] = 9;
		   _board[7][6] = 9;
		   _board[7][7] = 9.1;
		   _board[6][6] = -9;
		   _board[2][1] = 10;
		   _board[1][2] = 20; 
		   _board[2][3] = 30;
		   _board[3][2] = 40;
		   _board[2][4] = 50; 
		   _board[1][5] = 60;
		   _board[2][6] = 70;
		   _board[3][5] = 80;
		   _board[5][1] = 90;
		   _board[4][2] = 100;
		   _board[5][3] = 110;
		   _board[6][2] = 120;
		   _board[5][4] = 130;
		   _board[4][5] = 140;
		   _board[5][6] = 150;
		   _board[6][5] = 160;
	   }
	    @Override
		public void update() {
			updateGraphics();
			
		}
		
}


		

