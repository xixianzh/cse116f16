package edu.buffalo.cse116;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;


public class PlayerTest {
	private double[][] _board;
	private ArrayList<Card> _pCards;
	
    public void gridInitialization(){
	   _board = new double[8][8];
	   boolean pointless = true;
	   for(int i = 0; i < 8; i++){
		   _board[i][2] = 0;
		   _board[i][5] = 0;
		   _board[2][i] = 0;
		   _board[5][i] = 0;
	   }
	   _board[0][0] = 1.1; 
	   _board[0][1] = 1;
	   _board[1][0] = 1;
	   _board[1][1] = -1;
	   _board[0][3] = 2;
	   _board[0][4] = 2;
	   _board[1][4] = 2;
	   _board[1][3] = -2;
	   _board[0][6] = 3;
	   _board[0][7] = 3.1;
	   _board[1][7] = 3;
	   _board[1][6] = -3;
	   _board[3][0] = 4;
	   _board[3][1] = 4;
	   _board[4][0] = 4;
	   _board[4][1] = -4;
	   _board[3][3] = -51;
	   _board[3][4] = -52;
	   _board[4][3] = -53;
	   _board[4][4] = -54;
	   _board[3][7] = 6;
	   _board[4][7] = 6;
	   _board[4][6] = 6;
	   _board[3][6] = -6;
	   _board[6][0] = 7;
	   _board[7][0] = 7.1;
	   _board[7][1] = 7;
	   _board[6][1] = -7;
	   _board[6][3] = 8;
	   _board[7][3] = 8;
	   _board[7][4] = 8;
	   _board[6][4] = -8;
	   _board[6][7] = 9;
	   _board[7][6] = 9;
	   _board[7][7] = 9.1;
	   _board[6][6] = -9;
	   _board[2][1] = 10;
	   _board[1][2] = 20; 
	   _board[2][3] = 30;
	   _board[3][2] = 40;
	   _board[2][4] = 50; 
	   _board[1][5] = 60;
	   _board[2][6] = 70;
	   _board[3][5] = 80;
	   _board[5][1] = 90;
	   _board[4][2] = 100;
	   _board[5][3] = 110;
	   _board[6][2] = 120;
	   _board[5][4] = 130;
	   _board[4][5] = 140;
	   _board[5][6] = 150;
	   _board[6][5] = 160;
   }
	
	@Test
	public void test1(){ //tests various movements, both legal and illegal
		Player play1 = new Player(_board, 0, 2, null);
		assertEquals(true,play1.playerMove(1, 2));
		play1 = new Player(_board, 0, 2, null);
		assertEquals(false,play1.playerMove(0, 1));
		play1 = new Player(_board, 0, 2, null);
		assertEquals(false,play1.playerMove(0, 3));
		play1 = new Player(_board, 0, 2, null);
		assertEquals(false,play1.playerMove(1, 1));
		play1 = new Player(_board, 0, 2, null);
		assertEquals(false,play1.playerMove(1, 3));
	}
	@Test
	public void test2(){ //tests various movements, both legal and illegal
		Player play2 = new Player(_board, 2, 4, null);
		assertEquals(true,play2.playerMove(2, 3));
		play2 = new Player(_board, 2, 4, null);
		assertEquals(true,play2.playerMove(2, 5));
		play2 = new Player(_board, 2, 4, null);
		assertEquals(true,play2.playerMove(3, 4));
		play2 = new Player(_board, 2, 4, null);
		assertEquals(false,play2.playerMove(1, 3));
		play2 = new Player(_board, 2, 4, null);
		assertEquals(false,play2.playerMove(1, 4));
		play2 = new Player(_board, 2, 4, null);
		assertEquals(false,play2.playerMove(1, 5));
		play2 = new Player(_board, 2, 4, null);
		assertEquals(false,play2.playerMove(3, 3));
		play2 = new Player(_board, 2, 4, null);
		assertEquals(false,play2.playerMove(3, 5));
	}
	@Test
	public void test3(){ //tests various movements, both legal and illegal
		Player play3 = new Player(_board, 5, 3, null);
		assertEquals(true,play3.playerMove(4, 3));
		play3 = new Player(_board, 5, 3, null);
		assertEquals(true,play3.playerMove(5, 2));
		play3 = new Player(_board, 5, 3, null);
		assertEquals(true,play3.playerMove(5, 4));
		play3 = new Player(_board, 5, 3, null);
		assertEquals(false,play3.playerMove(4, 2));
		play3 = new Player(_board, 5, 3, null);
		assertEquals(false,play3.playerMove(4, 4));
		play3 = new Player(_board, 5, 3, null);
		assertEquals(false,play3.playerMove(6, 2));
		play3 = new Player(_board, 5, 3, null);
		assertEquals(false,play3.playerMove(6, 3));
		play3 = new Player(_board, 5, 3, null);
		assertEquals(false,play3.playerMove(6, 4));
	}
	@Test
	public void test4(){ //tests various movements, both legal and illegal
		Player play4 = new Player(_board, 7, 5, null);
		assertEquals(true,play4.playerMove(6, 5));
		play4 = new Player(_board, 7, 5, null);
		assertEquals(false,play4.playerMove(6, 4));
		play4 = new Player(_board, 7, 5, null);
		assertEquals(false,play4.playerMove(6, 6));
		play4 = new Player(_board, 7, 5, null);
		assertEquals(false,play4.playerMove(7, 4));
		play4 = new Player(_board, 7, 5, null);
		assertEquals(false,play4.playerMove(7, 6));
	}
	@Test
	public void test5(){ //tests various movements, both legal and illegal
		Player play5 = new Player(_board, 4, 6, null);
		assertEquals(true,play5.playerMove(3, 6));
		play5 = new Player(_board, 4, 6, null);
		assertEquals(false,play5.playerMove(3, 5));
		play5 = new Player(_board, 4, 6, null);
		assertEquals(false,play5.playerMove(4, 5));
		play5 = new Player(_board, 4, 6, null);
		assertEquals(false,play5.playerMove(5, 5));
		play5 = new Player(_board, 4, 6, null);
		assertEquals(false,play5.playerMove(5, 6));
		play5 = new Player(_board, 4, 6, null);
		assertEquals(false,play5.playerMove(5, 7));
	}
	@Test
	public void test6(){ //tests various movements, both legal and illegal
		Player play5 = new Player(_board, 5, 1, null);
		assertEquals(true,play5.playerMove(4, 1));
		play5 = new Player(_board, 5, 1, null);
		assertEquals(true,play5.playerMove(5, 0));
		play5 = new Player(_board, 5, 1, null);
		assertEquals(true,play5.playerMove(5, 2));
		play5 = new Player(_board, 5, 1, null);
		assertEquals(true,play5.playerMove(6, 1));
		play5 = new Player(_board, 5, 1, null);
		assertEquals(false,play5.playerMove(4, 0));
		play5 = new Player(_board, 5, 1, null);
		assertEquals(false,play5.playerMove(4, 2));
		play5 = new Player(_board, 5, 1, null);
		assertEquals(false,play5.playerMove(5, 1));
		play5 = new Player(_board, 5, 1, null);
		assertEquals(false,play5.playerMove(6, 0));
		play5 = new Player(_board, 5, 1, null);
		assertEquals(false,play5.playerMove(6, 2));
	}
    @Test
	public void test7(){ //tests that we only move the number we roll horizontally
		Player play = new Player(_board, 2, 2, null);
		play.playerMove(2,3);
		play.playerMove(2,4);
		play.playerMove(2,5);
		assertEquals(2, play.getr1());
		assertEquals(5, play.getc1());
    }
	@Test
	public void test8(){ //tests that we only move the number we roll vertically
		Player play = new Player(_board, 5, 5, null);
		play.playerMove(4,5);
		play.playerMove(3,5);
		play.playerMove(2,5);
		assertEquals(2, play.getr1());
		assertEquals(5, play.getc1());
	}
	@Test
	public void test9(){ //tests that we only move the number we roll horizontally and vertically
		Player play = new Player(_board, 6, 2, null);
		play.playerMove(5,2);
		play.playerMove(5,3);
		play.playerMove(5,4);
		play.playerMove(5,5);
		play.playerMove(6,5);
		assertEquals(6, play.getr1());
		assertEquals(5, play.getc1());
	}
	@Test
	public void test10(){ //tests moving hallway -> door -> room
		Player play = new Player(_board, 4, 2, null);
		assertTrue(play.playerMove(4, 1));
		assertTrue(play.playerMove(4, 0));
	}
	@Test
	public void test11(){ //tests moving through secret passageway
		Player play = new Player(_board, 0, 7, null);
		assertTrue(play.playerMove(7, 0));
		assertFalse(play.playerMove(7, 7));
	}
	@Test
	public void test12(){ 
		/*we did not need to test that it is illegal to move more squares than
		*die roll because we only call the method as many times as the number that
		*we roll
		*/	
	}
	@Test
	public void test13(){ //tests diagonal movement
		/*test no longer needed because our movement is controlled by up/down/left/right buttons
		*(player cannot choose to move diagonally)
		*/
	}
	@Test
	public void test14(){
		/*we did not need to test that it is illegal to move non-contiguously
		 * because our input will only allow movement to adjacent square
		 */
	}
	@Test
	public void test15(){ //tests movement through a wall
		Player play5 = new Player(_board, 3, 2, null);
		assertFalse(play5.playerMove(3, 1));
		play5 = new Player(_board, 2, 7, null); 
		assertFalse(play5.playerMove(1, 7));
		play5 = new Player(_board, 5, 3, null); 
		assertFalse(play5.playerMove(6, 3));
	}
	@Test
	public void test16(){ //tests that the cardShow method works properly (returns a card that is in the other player's hand)
		GameBoardGUI g = new GameBoardGUI();
		g.startLocation();
		g.cardCreate();
		g.solutionCards();
		_pCards = new ArrayList<Card>();
		_pCards.add(new Card("weapon", "rope"));
		_pCards.add(new Card("weapon", "wrench"));
		_pCards.add(new Card("weapon", "candlestick"));
		_pCards.add(new Card("weapon", "lead pipe"));
		_pCards.add(new Card("suspect", "Miss Scarlet"));
		_pCards.add(new Card("room", "study"));
		Card w = new Card("weapon", "revolver");
		Card s = new Card("suspect", "Colonel Mustard");
		Card r = new Card ("room", "conservatory");
		Player p = new Player(_board, 3, 7, _pCards);
		assertEquals(_pCards.get(0),p.cardShow(_pCards.get(0), r, s)); //_pCards.get(0), r, and s are the cards guessed by the other player
		assertNull(p.cardShow(r, w, s));
	}
	@Test
	public void testNextPlayerWeapon(){ //test that ensure next player reveals, due to having weapon card
		GameBoardGUI g = new GameBoardGUI();
		ArrayList<Card> pCards = new ArrayList<Card>();
		pCards.add(new Card("weapon", "rope"));
		pCards.add(new Card("weapon", "wrench"));
		pCards.add(new Card("weapon", "candlestick"));
		pCards.add(new Card("weapon", "lead pipe"));
		pCards.add(new Card("suspect", "Miss Scarlet"));
		pCards.add(new Card("room", "study"));
		Player p1 = new Player(_board, 3, 7, null);
		p1.setIndex(1);
		Player p2 = new Player(_board, 4, 2, pCards);
		p2.setIndex(2);
		g.setPlayer1(p1);
		g.setPlayer2(p2);
		Card w = new Card("weapon", "rope");
		Card s = new Card("suspect", "Colonel Mustard");
		Card r = new Card ("room", "conservatory");
		assertEquals(2, g.playerReveal(p1, r, w, s)); //2 is the index of the player that is returned
		
	}
	@Test
	public void testNextPlayerRoom(){ //tests that ensure next player reveals, due to having room card
		GameBoardGUI g = new GameBoardGUI();
		ArrayList<Card> pCards = new ArrayList<Card>();
		pCards.add(new Card("weapon", "rope"));
		pCards.add(new Card("weapon", "wrench"));
		pCards.add(new Card("weapon", "candlestick"));
		pCards.add(new Card("weapon", "lead pipe"));
		pCards.add(new Card("suspect", "Miss Scarlet"));
		pCards.add(new Card("room", "study"));
		Player p1 = new Player(_board, 3, 7, null);
		p1.setIndex(1);
		Player p2 = new Player(_board, 4, 2, pCards);
		p2.setIndex(2);
		g.setPlayer1(p1);
		g.setPlayer2(p2);
		Card w = new Card("weapon", "knife");
		Card s = new Card("suspect", "Colonel Mustard");
		Card r = new Card ("room", "study");
		assertEquals(2, g.playerReveal(p1, r, w, s));
	}
	@Test
	public void testNextPlayerSuspect(){//tests that ensure next player reveals, due to suspect card
		GameBoardGUI g = new GameBoardGUI();
		ArrayList<Card> pCards = new ArrayList<Card>();
		pCards.add(new Card("weapon", "rope"));
		pCards.add(new Card("weapon", "wrench"));
		pCards.add(new Card("weapon", "candlestick"));
		pCards.add(new Card("weapon", "lead pipe"));
		pCards.add(new Card("suspect", "Miss Scarlet"));
		pCards.add(new Card("room", "study"));
		Player p1 = new Player(_board, 3, 7, null);
		p1.setIndex(1);
		Player p2 = new Player(_board, 4, 2, pCards);
		p2.setIndex(2);
		g.setPlayer1(p1);
		g.setPlayer2(p2);
		Card w = new Card("weapon", "knife");
		Card s = new Card("suspect", "Miss Scarlet");
		Card r = new Card ("room", "library");
		assertEquals(2, g.playerReveal(p1, r, w, s));
	}
	@Test
	public void testNextPlayerHas2(){//tests that ensure next player reveals, b/c they have 2 cards
		GameBoardGUI g = new GameBoardGUI();
		ArrayList<Card> pCards = new ArrayList<Card>();
		pCards.add(new Card("weapon", "rope"));
		pCards.add(new Card("weapon", "wrench"));
		pCards.add(new Card("weapon", "candlestick"));
		pCards.add(new Card("weapon", "lead pipe"));
		pCards.add(new Card("suspect", "Miss Scarlet"));
		pCards.add(new Card("room", "study"));
		Player p1 = new Player(_board, 3, 7, pCards);
		p1.setIndex(1);
		Player p2 = new Player(_board, 4, 2, null);
		p2.setIndex(2);
		g.setPlayer1(p1);
		g.setPlayer2(p2);
		Card w = new Card("weapon", "rope");
		Card s = new Card("suspect", "Miss Scarlet");
		Card r = new Card ("room", "library");
		assertEquals(1, g.playerReveal(p2, r, w, s));
	}
	@Test
	public void testPlayerAfterNextPlayer(){ //tests that ensure player after next player reveals
		//Our game is only being implemented with 2 players, so this test is unnecessary
	}
	@Test
	public void testLastPlayer(){ //tests that ensure the last possible player reveals
		//Our game is only being implemented with 2 players, so this test is unnecessary
	}
	@Test
	public void nobodyRevealsButIHaveOne(){//tests that ensure nobody reveals, but guesser has a card
		GameBoardGUI g = new GameBoardGUI();
		ArrayList<Card> pCards = new ArrayList<Card>();
		ArrayList<Card> xCards = new ArrayList<Card>();
		
		pCards.add(new Card("weapon", "rope"));
		pCards.add(new Card("weapon", "wrench"));
		pCards.add(new Card("weapon", "candlestick"));
		pCards.add(new Card("weapon", "lead pipe"));
		pCards.add(new Card("suspect", "Miss Scarlet"));
		pCards.add(new Card("room", "study"));
		
		xCards.add(new Card("weapon", "knife"));
		xCards.add(new Card("suspect", "Professor Plum"));
		xCards.add(new Card("suspect", "Mr Green"));
		xCards.add(new Card("room", "conservatory"));
		xCards.add(new Card("room", "library"));
		xCards.add(new Card("room", "kitchen"));
		
		
		Player p1 = new Player(_board, 3, 7, pCards);
		p1.setIndex(1);
		Player p2 = new Player(_board, 4, 2, xCards);
		p2.setIndex(2);
		g.setPlayer1(p1);
		g.setPlayer2(p2);
		
		Card w = new Card("weapon", "rope");
		Card s = new Card("suspect", "Mrs White");
		Card r = new Card ("room", "study");
		
		assertEquals(0, g.playerReveal(p1, r, w, s)); //nobody else has this card
		assertNotNull(p1.cardShow(r,w,s));	//but the guessing player does have one of them
	}
	@Test
	public void nobodyRevealsNobodyHas(){//tests that ensure nobody reveals, and nobody has it
		GameBoardGUI g = new GameBoardGUI();
		ArrayList<Card> pCards = new ArrayList<Card>();
		ArrayList<Card> xCards = new ArrayList<Card>();
		
		pCards.add(new Card("weapon", "rope"));
		pCards.add(new Card("weapon", "wrench"));
		pCards.add(new Card("weapon", "candlestick"));
		pCards.add(new Card("weapon", "lead pipe"));
		pCards.add(new Card("suspect", "Miss Scarlet"));
		pCards.add(new Card("room", "study"));
		
		xCards.add(new Card("weapon", "knife"));
		xCards.add(new Card("suspect", "Professor Plum"));
		xCards.add(new Card("suspect", "Mr Green"));
		xCards.add(new Card("room", "conservatory"));
		xCards.add(new Card("room", "library"));
		xCards.add(new Card("room", "kitchen"));
		
		
		Player p1 = new Player(_board, 3, 7, pCards);
		p1.setIndex(1);
		Player p2 = new Player(_board, 4, 2, xCards);
		p2.setIndex(2);
		g.setPlayer1(p1);
		g.setPlayer2(p2);
		
		Card w = new Card("weapon", "revolver");
		Card s = new Card("suspect", "Colonel Mustard");
		Card r = new Card ("room", "billiard room");
		
		assertEquals(0, g.playerReveal(p1, r, w, s)); //nobody else has this card
		assertNull(p1.cardShow(r,w,s));	//the guessing player also does not have this card
	}
	
} 
